import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AlertesPage } from './alertes.page';

import { AlertesPageRoutingModule } from './alertes-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    AlertesPageRoutingModule
  ],
  declarations: [AlertesPage]
})
export class AlertesPageModule {}
