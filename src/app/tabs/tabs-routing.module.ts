import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'saisie',
        loadChildren: () => import('../saisie/saisie.module').then(m => m.SaisiePageModule)
      },
      {
        path: 'configuration',
        loadChildren: () => import('../configuration/configuration.module').then(m => m.ConfigurationPageModule)
      },
      {
        path: 'alertes',
        loadChildren: () => import('../alertes/alertes.module').then(m => m.AlertesPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/saisie',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/saisie',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
