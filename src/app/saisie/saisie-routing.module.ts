import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SaisiePage } from './saisie.page';

const routes: Routes = [
  {
    path: '',
    component: SaisiePage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaisiePageRoutingModule {}
