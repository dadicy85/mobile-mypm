import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SaisiePage } from './saisie.page';

import { SaisiePageRoutingModule } from './saisie-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    SaisiePageRoutingModule
  ],
  declarations: [SaisiePage]
})
export class SaisiePageModule {}
