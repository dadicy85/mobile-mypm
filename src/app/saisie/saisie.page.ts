import { Component } from '@angular/core';
import { format, addDays } from 'date-fns';

interface Task {
  name: string;
  blocks: number;
}

@Component({
  selector: 'app-saisie',
  templateUrl: 'saisie.page.html',
  styleUrls: ['saisie.page.scss'],
})

export class SaisiePage {

  currentDate = new Date();
  days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
  weekDates: Date[] = [];
  imputedTasks: { [day: string]: Task[] } = {};
  selectedDay: string = this.days[0];

  tasks: Task[] = [
    { name: 'Support IC', blocks: 0.5 },
    { name: 'Tâche 2', blocks: 0.3 },
    { name: 'Tâche 3', blocks: 0.2 },
    { name: 'Pilotage RUN', blocks: 1.0 },
    { name: 'Tâche 5', blocks: 0.8 },
    { name: 'Tâche 6', blocks: 0.1 },
    { name: 'Tâche 7', blocks: 0.6 },
    { name: 'Tâche 8', blocks: 0.2 },
    { name: 'Tâche 9', blocks: 0.4 },
    { name: 'Tâche 10', blocks: 0.3 }
  ];

  constructor() {
    this.generateWeekDates();
  }

  generateWeekDates() {
    const start = new Date(this.currentDate);
    start.setDate(start.getDate() - start.getDay());
    for (let i = 0; i < 7; i++) {
      const date = new Date(start);
      date.setDate(date.getDate() + i);
      this.weekDates.push(date);
      this.imputedTasks[this.formatDate(date)] = [];
    }
  }

  formatDate(date: Date): string {
    return date.toLocaleDateString('fr-FR');
  }

  addTask(task: Task) {
    const imputedTasks = this.imputedTasks[this.formatDate(this.weekDates[0])];
    const totalBlocks = imputedTasks.reduce((sum: number, t: Task) => sum + t.blocks, 0);

    if (totalBlocks + task.blocks <= 1.0) {
      imputedTasks.push(task);
      console.log(`La tâche ${task.name} a été ajoutée à ${this.selectedDay}.`);
    } else {
      console.log(`Votre journée est déjà pleine. Si vous voulez imputer une tâche, veuillez réduire ou supprimer une autre tâche.`);
    }
  }

  isTaskImputed(day: string, task: Task): boolean {
    const imputedTasks = this.imputedTasks[this.formatDate(this.weekDates[0])];
    return imputedTasks.some((t: Task) => t.name === task.name && day === this.selectedDay);
  }
}
