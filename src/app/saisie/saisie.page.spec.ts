import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SaisiePage } from './saisie.page';

describe('SaisiePage', () => {
  let component: SaisiePage;
  let fixture: ComponentFixture<SaisiePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SaisiePage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SaisiePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
